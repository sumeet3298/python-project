from IPython.display import clear_output
import os
import random

#display board func

def display_board(board):
    #clear_output()
    os.system('cls')
    print(board[7]+'|'+board[8]+'|'+board[9])
    print('-----')
    print(board[4]+'|'+board[5]+'|'+board[6])
    print('-----')
    print(board[1]+'|'+board[2]+'|'+board[3])
    print('-----')
test_board=['#','X','O','X','O','X','O','X','O','X']
#test_board=['']*10
#display_board(test_board)

#user inputs func
def player_input():
    marker=''
    while marker!='X' and marker!='O':
        marker=input('Player 1: Chose X or O:').upper()

    if marker=='X':
        return ('X','O')
    else:
        return ('O','X')


#p1mark,p2mark=player_input()
#print(p1mark,p2mark)

#place the marker in the chosen position
def place_marker(board,marker,position):
    board[position]=marker

#decide winning marker

def win_check(board,mark):
    return ((board[1]==board[2]==board[3]==mark or
    board[4]==board[5]==board[6]==mark or
    board[7]==board[8]==board[9]==mark or
    board[1]==board[4]==board[7]==mark or
    board[2]==board[5]==board[8]==mark or
    board[3]==board[6]==board[9]==mark or
    board[1]==board[5]==board[9]==mark or
    board[3]==board[5]==board[7]==mark ))

#bol=win_check(test_board,'$')
#print(bol)

def choose_first():
    flip=random.randint(0,1)

    if flip==0:
        return 'Player 1'
    else:
        return 'Player 2'

def space_check(board,position):
    return board[position]==' '

def full_board_check(board):
    for i in range(1,10):
        if space_check(board,i):
            return False
    #BOARD IS FULL IF WE RETURN TRUE
    return True

def player_choice(board):

    position=0
    while position not in [1,2,3,4,5,6,7,8,9] or not space_check(board,position):
        position=int(input('Chose a position (1-9)'))
    return position

def replay():
    choice=input("Play Again ? Enter Yes or No:")
    return choice =='Yes'

## WHILE loop to keep running the game!

print(' Play the GAME TIK TAK TOE !!!')

while True:

    ##Setup the board .
    the_board=[' ']*10

    ##Get player markers
    play1mark,play2mark=player_input()

    turn=choose_first()
    print(turn+' will go first')

    play_game=input('Ready to play? Press y or n')
    if play_game == 'y':
        game_on =True
    else:
        game_on=False

    ##START GAME
    while game_on:
        if turn=='Player 1':
            #Player 1 goes
            #show th board
            display_board(the_board)
            #Chose the position
            position=player_choice(the_board)

            #Place thr marker on the position
            place_marker(the_board,play1mark,position)

            #Check if they won
            if win_check(the_board,play1mark):
                display_board(the_board)
                print('Player 1 has won!!')
                game_on=False
            else:
                if full_board_check(the_board):
                    display_board(the_board)
                    print('The game is a TIE!!')
                    game_on=False
                else:
                    turn='Player 2'


        else:
            #Player 2 goes
            display_board(the_board)
            #Chose the position
            position=player_choice(the_board)

            #Place thr marker on the position
            place_marker(the_board,play2mark,position)

            #Check if they won
            if win_check(the_board,play2mark):
                display_board(the_board)
                print('Player 2 has won!!')
                game_on=False
            else:
                if full_board_check(the_board):
                    display_board(the_board)
                    print('The game is a TIE!!')
                    game_on=False
                else:
                    turn='Player 1'

    if not replay():
        break
## Break out of the while loop on replay ()

import time
from datetime import datetime as dt

'''
This program adds lists of hosts present under C:\Windows\System32\drivers\etc\hosts file if the host are in working hours.
Working hours defined in program is between 08:00 AM to 16:00 PM
Once the program detects time outside working hours it removes the blocked_hosts from the hosts file.

'''

hosts_path =r"C:\Windows\System32\drivers\etc\hosts"    #using temporary location to test the program
hosts_temp = r"C:\Users\sumeet\Desktop\Python_training\projects\python-project\Website Blocker\hosts" #Windows hosts file location
redirect='127.0.0.1' #redirect to localhost to block access.
website_list=["www.facebook,com","facebook.com","youtube.com","www.youtube.com"] #list of website which needs to be blocked.

#While loop to run it continously every 10 secs.
while True:
    if dt(dt.now().year,dt.now().month,dt.now().day,8) < dt.now() < dt(dt.now().year,dt.now().month,dt.now().day,16):
        print("Time is in working hours!!")
        with open(hosts_temp,'r+') as file:
            content=file.read()
            for website in website_list:
                if website in content:
                    pass
                else:
                    file.write(redirect+" "+website+"\n")

    else:
        print("Time is in non working hours")
        with open(hosts_temp,'r+') as file:
            content=file.readlines()
            file.seek(0)
            for line in content:
                if not any(website in line for website in website_list):  #use of list comprehension here.
                    file.write(line)
            file.truncate()

    time.sleep(10)
